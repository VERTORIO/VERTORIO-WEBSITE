#!/bin/bash
# Update Date Script
# @author Denis Zholob (deniszholob.com)
# Writes the current date to file

echo 'Update Date script started...'
now="$(date +'%Y/%m/%d')"
file="src/assets/js/update.js"
printf "const vertorioUpdateDate = '%s';\n" "$now" > $file
echo 'Update Date script finished...'
