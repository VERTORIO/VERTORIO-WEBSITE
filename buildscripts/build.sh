#!/bin/bash
# Website build script
# @author Denis Zholob (deniszholob.com)
#
# Calls updateDate script
# Copies src to public folder that gitlab uses to host the static website
# ====================================== #

echo 'Build Script Started ...'
sh buildscripts/updateDate.sh
mv src public
echo 'Update Date script finished...'
