/** 
 * Vertorio Main Javasctipt File
 * @author Denis Zholob (deniszholob.com)
 */

/** Main entry function when the body loads */
function onVertorioLoad() {
    setUpdateDate();
}

/** Sets the update date in the footer to the variable in the update.js file */
function setUpdateDate() {
    const el = document.getElementById('UpdateDate');
    el.innerHTML = vertorioUpdateDate;
}
